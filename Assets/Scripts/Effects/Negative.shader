﻿Shader "Hidden/Negative"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
      float _Intensity;

      fixed4 lerpColor(fixed4 c1, fixed4 c2, float lerp)
      {
          float l1 = 1 - lerp;
          float l2 = lerp;
          return c1*l1 + c2*l2;
      }

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col1 = tex2D(_MainTex, i.uv);
        fixed4 col2 = 1 - col1;
				return lerpColor(col1, col2, _Intensity);
			}
			ENDCG
		}
	}
}
