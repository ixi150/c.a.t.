﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerPrefBool = PlayerPrefsExtensions.BoolPlayerPref;

[ExecuteInEditMode]
public class NegativeEffect : MonoBehaviour
{
    [SerializeField]
    Shader shader;

    [SerializeField, Range(0, 1)]
    float intensity = 0;

    public static void Play()
    {
        Ref.PlayLocal();
    }

    public void PlayLocal()
    {
        if (!IsEpilepticSafe)
        {
            Ref.animator.SetTrigger("Negative");
        }
    }

    public static bool IsEpilepticSafe
    {
        get { return epilepticSafe.Value; }
        set { epilepticSafe.Value = value; }
    }

    static PlayerPrefBool epilepticSafe = new PlayerPrefBool("EpilepticSafe", false);

    static NegativeEffect Ref { get; set; }
    Material material;
    Animator animator;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null)
        {
            if (shader == null) return;
            material = new Material(shader);
        }

        material.SetFloat("_Intensity", intensity);
        Graphics.Blit(source, destination, material);
    }

    private void Awake()
    {
        Ref = this;
        animator = GetComponent<Animator>();
    }
}
