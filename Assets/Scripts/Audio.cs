﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BoolPlayerPref = PlayerPrefsExtensions.BoolPlayerPref;

public class Audio : MonoBehaviour
{
    public static bool SoundMuted
    {
        get { return soundMuted.Value; }
        set { soundMuted.Value = value; }
    }
    public static bool MusicMuted
    {
        get { return musicMuted.Value; }
        set { musicMuted.Value = value; }
    }

    static BoolPlayerPref soundMuted = new BoolPlayerPref("SoundMute");
    static BoolPlayerPref musicMuted = new BoolPlayerPref("MusicMute");
    const string
        PrefsSoundVolume = "SoundVolume",
        PrefsMusicVolume = "MusicVolume";
}


