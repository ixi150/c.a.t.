﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HintButtonHover : MonoBehaviour, IPointerOverHandler, IPointerEnterHandler
{
    private ImageScaller _image;

    protected void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.tag == "Image")
            {
                _image = child.GetComponent<ImageScaller>();
                _image.transform.SetParent(transform.parent);
            }
        }

        if (!_image)
            Debug.LogError("Image object not found!!");
    }

    public virtual void OnPointerHover(HoverPointerEventData data)
    {
        try
        {
            Expand();
            GUIPanel.ItemPanel.HidePanel();
        }
        catch
        {
            Debug.LogError("Image object not found!!");
        }
    }

    public void Expand()
    {
        _image.FocusImage();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        try
        {
            Expand();
            GUIPanel.ItemPanel.HidePanel();
        }
        catch
        {
            Debug.LogError("Image object not found!!");
        }
    }
}
