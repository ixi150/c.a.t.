﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIPanel : MonoBehaviour, IPointerEnterHandler
{
    public static GUIPanel ItemPanel { get; private set; }
    private readonly string _onScreen = "OnScreen";

    private Animator _animator;

    private void Awake()
    {
        ItemPanel = this;
        _animator = GetComponent<Animator>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _animator.SetBool(_onScreen, true);
    }

    public void HidePanel()
    {
        try
        {
            _animator.SetBool(_onScreen, false);
        }
        catch
        {
            Debug.LogError("ItemPanel  not found");
        }
    }
}
