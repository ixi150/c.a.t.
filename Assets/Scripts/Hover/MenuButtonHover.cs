﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuButtonHover : DelayedHoverClickIndicator, IPointerClickHandler, ISubmitHandler
{
    public GameObject menu;

    public void OnPointerClick(PointerEventData eventData)
    {
        OnSubmit(eventData);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        if (menu)
            menu.gameObject.SetActive(true);
    }
}
