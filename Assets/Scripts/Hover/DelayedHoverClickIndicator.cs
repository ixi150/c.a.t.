﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DelayedHoverClickIndicator : MonoBehaviour, IPointerOverHandler
{
    public bool hideItemPanelOnFocus = true;

    private Image _focus;

    private void OnEnable()
    {
        if (_focus) _focus.fillAmount = 0;
    }

    protected void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.tag == "Focus")
            {
                _focus = child.GetComponent<Image>();
                _focus.fillAmount = 0;
            }
        }

        if (!_focus)
            Debug.LogError("Focus not object found!!");
    }

    public virtual void OnPointerHover(HoverPointerEventData data)
    {
        try
        {
            Fill(data.Progress);
            if (hideItemPanelOnFocus)
                GUIPanel.ItemPanel.HidePanel();
            ImageScaller.UnfocusAll();

        }
        catch
        {
            Debug.LogError("Focus not object found!!");
        }
    }

    public void Fill(float amount)
    {
        _focus.fillAmount = amount;
    }
}
