﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuStageSelectScreen : MenuScreen
{
    const string GameScenePrefix = "Game ";

    //public string mainMenuScene;
    [SerializeField]
    GameObject mainMenuGO;

    public void ButtonBack()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: LoadMainMenu,
            finishCallback: FinishedChanging);
    }

    public void ButtonLoadGame(int stageNumber)
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: () => { LoadGame(stageNumber); },
            finishCallback: FinishedChanging);
    }

    void LoadMainMenu()
    {
        //LoadScene(mainMenuScene);
        ToggleOtherGameObject(mainMenuGO);
    }

    void LoadGame(int stageNumber)
    {
        LoadScene(GameScenePrefix + stageNumber);
    }
}