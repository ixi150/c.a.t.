﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScreenUI : MenuScreen
{
    [SerializeField]
    GameObject optionsGO;

    public void ButtonOptions()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: LoadOptions,
            finishCallback: () => { Debug.Log("Loaded Options"); });
    }

    public void ButtonBack()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: LoadMenu);
    }

    void LoadMenu()
    {
        LoadScene("MainMenu");
    }

    void LoadOptions()
    {
        ToggleOtherGameObject(optionsGO);
    }
}
