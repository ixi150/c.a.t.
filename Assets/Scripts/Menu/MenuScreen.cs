﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScreen : MonoBehaviour
{
    public static void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    protected static void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {
        GetComponent<Canvas>().worldCamera = CameraController.Ref.GetComponent<Camera>();
    }

    protected virtual void StartedChanging()
    {
        Debug.Log("Started changing: " + this.GetType().ToString(), this);
    }

    protected virtual void FinishedChanging()
    {
        Debug.Log("Finished changing: "+ this.GetType().ToString(), this);
    }

    protected void ToggleOtherGameObject(GameObject other)
    {
        if (other)
        {
            other.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
