﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLaunchScreen : MenuScreen
{
    //public string playScene, optionsScene;
    [SerializeField]
    GameObject stageSelectGO, optionsGO;

    public void ButtonPlay()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: LoadPlay,
            finishCallback: () => { Debug.Log("Loaded Play"); },
            delay: .5f);
    }

    public void ButtonOptions()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: LoadOptions,
            finishCallback: () => { Debug.Log("Loaded Options"); });
    }

    public void ButtonQuit()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: Quit);
    }

    void LoadPlay()
    {
        //LoadScene(playScene);
        ToggleOtherGameObject(stageSelectGO);
    }

    void LoadOptions()
    {
        //LoadScene(optionsScene);
        ToggleOtherGameObject(optionsGO);
    }
}
