﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour
{
    readonly static Color clearColor = Color.clear;

    [SerializeField]
    float defaultDuration = 1.5f;

    public Color fillColor = Color.black;

    public static void Fadeout(float duration = -1, Action halfwayCallback = null, Action finishCallback = null, float delay = 0)
    {
        if (Ref == null)
        {
            InitializeFader();
        }
        Ref.StartFadingCoroutine(duration, halfwayCallback, finishCallback, delay);
    }

    static void InitializeFader()
    {
        Instantiate(Resources.Load("Prefabs/FadeoutCanvas", typeof(GameObject)));
    }

    static ScreenFader Ref { get; set; }
    Coroutine coroutine;
    Image image;

    private void Awake()
    {
        if (Ref)
        {
            Destroy(transform.parent.gameObject);
            return;
        }
        Ref = this;
        image = GetComponent<Image>();
    }

    void StartFadingCoroutine(float duration, Action halfwayCallback, Action finishCallback, float delay)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        if (duration < 0)
            duration = defaultDuration;
        coroutine = StartCoroutine(StartFading(duration, halfwayCallback, finishCallback, delay));
    }


    IEnumerator StartFading(float duration, Action halfwayCallback, Action finishCallback, float delay)
    {
        image.raycastTarget = true;
        yield return new WaitForSeconds(delay);

        float timer = 0;
        float halfDuration = duration / 2;


        while (timer < halfDuration)
        {
            timer += Time.deltaTime;
            UpdateColor(timer / halfDuration);
            yield return null;
        }

        image.raycastTarget = false;
        if (halfwayCallback != null)
            halfwayCallback();

        while (timer > 0)
        {
            timer -= Time.deltaTime;
            UpdateColor(timer / halfDuration);
            yield return null;
        }

        if (finishCallback != null)
            finishCallback();
    }

    void UpdateColor(float percentFaded)
    {
        image.color = Color.Lerp(clearColor, fillColor, percentFaded);
    }
}
