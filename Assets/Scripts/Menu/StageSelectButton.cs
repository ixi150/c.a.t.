﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSelectButton : MonoBehaviour
{
    [SerializeField, Range(1,8)]
    int stageNumber = 1;

	public void OnClick()
    {
        FindObjectOfType<MenuStageSelectScreen>().ButtonLoadGame(stageNumber);
    }
}
