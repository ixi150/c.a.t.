﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayerPrefBool = PlayerPrefsExtensions.BoolPlayerPref;

public class MenuOptionsScreen : MenuScreen
{
    [SerializeField]
    GameObject mainMenuGO;

    [SerializeField]
    Image crossSoundMute, crossMusicMute, crossBloom, crossEye, crossBG, crossEpileptic;

    static PlayerPrefBool eyeTrackingOn = new PlayerPrefBool("EyeTracking", true);

    public void ButtonBack()
    {
        StartedChanging();
        ScreenFader.Fadeout(halfwayCallback: LoadMainMenu,
            finishCallback: FinishedChanging);
    }

    public void ButtonSound()
    {
        Audio.SoundMuted = !Audio.SoundMuted;
        RefreshCrossesOnButtons();
    }

    public void ButtonMusic()
    {
        Audio.MusicMuted = !Audio.MusicMuted;
        RefreshCrossesOnButtons();
    }

    public void ButtonBloom()
    {
        CameraController.BloomEnabled = !CameraController.BloomEnabled;
        RefreshCrossesOnButtons();
    }

    public void ButtonEye()
    {
        eyeTrackingOn.Value = !eyeTrackingOn.Value;
        RefreshCrossesOnButtons();
    }

    public void ButtonBG()
    {
        Background.Enabled = !Background.Enabled;
        RefreshCrossesOnButtons();
    }

    public void ButtonEpileptic()
    {
        NegativeEffect.IsEpilepticSafe = !NegativeEffect.IsEpilepticSafe;
        RefreshCrossesOnButtons();
    }

    protected override void Awake()
    {
        base.Awake();
        RefreshCrossesOnButtons();
    }

    void LoadMainMenu()
    {
        ToggleOtherGameObject(mainMenuGO);
    }

    void RefreshCrossesOnButtons()
    {
        crossSoundMute.enabled = Audio.SoundMuted;
        crossMusicMute.enabled = Audio.MusicMuted;
        crossBloom.enabled = !CameraController.BloomEnabled;
        crossEye.enabled = !eyeTrackingOn.Value;
        TobiiInputModule.Ref.Enabled = eyeTrackingOn.Value;
        crossBG.enabled = !Background.Enabled;
        crossEpileptic.enabled = !NegativeEffect.IsEpilepticSafe;
    }
}