﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public enum SubMenu
    {
        main, select, options, win
    }

    [SerializeField]
    GameObject[] subMenuGOs;

    public static void SetActiveSubmenu(SubMenu submenu)
    {
        activeSubMenu = submenu;
    }

    static SubMenu activeSubMenu = SubMenu.main;

    //static MenuManager Ref { get; set; }
    private void Awake()
    {
        ImageScaller.Scaller.Clear();
        //Ref = this;
    }

    private void Start()
    {
        for (int i = 0; i < subMenuGOs.Length; i++)
        {
            subMenuGOs[i].SetActive(i == (int)activeSubMenu);
        }
    }

}
