﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerPrefBool = PlayerPrefsExtensions.BoolPlayerPref;

public class Background : MonoBehaviour
{
    public static bool Enabled
    {
        get { return backgroundOn.Value; }
        set
        {
            backgroundOn.Value = value;
            foreach (var bg in list)
            {
                if (bg) bg.RefreshActiveness();
            }
        }
    }

    static PlayerPrefBool backgroundOn = new PlayerPrefBool("BackGroundOn", true);
    readonly static List<Background> list = new List<Background>();

    private void Awake()
    {
        list.Add(this);
    }

    void Start ()
    {
        RefreshActiveness();
    }

    private void OnDestroy()
    {
        list.Remove(this);
    }

    void RefreshActiveness()
    {
        gameObject.SetActive(Enabled);
    }
}
