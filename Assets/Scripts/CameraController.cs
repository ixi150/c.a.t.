﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraController : MonoBehaviour
{
    public static CameraController Ref;

    public static bool BloomEnabled
    {
        get { return bloomEnabled.Value; }
        set { bloomEnabled.Value = value; Ref.RefreshBloomValue(); }
    }
    static PlayerPrefsExtensions.BoolPlayerPref bloomEnabled 
        = new PlayerPrefsExtensions.BoolPlayerPref("BloomEnabled", defaultValue: true);

    PostProcessingBehaviour postProcess;

    private void Awake()
    {
        if (Ref)
        {
            Destroy(gameObject);
            return;
        }
        Ref = this;
        postProcess = GetComponent<PostProcessingBehaviour>();
        postProcess.profile = Instantiate(postProcess.profile);
        RefreshBloomValue();
    }

    void RefreshBloomValue()
    {
        postProcess.profile.bloom.enabled = BloomEnabled;
    }
}
