﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ImageScaller : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public static readonly List<ImageScaller> Scaller = new List<ImageScaller>();
    public AnimationCurve expandCurve;
    public float expandMultiply = 0.1f;
    public float colapseMultiply = 2;

    private Vector3 _startScale;
    private bool _isFocused = false;
    private float _percent;

    private void Awake()
    {
        Scaller.Add(this);
        _startScale = transform.localScale;
        transform.localScale = Vector3.zero;
        gameObject.SetActive(true);
    }

    private void Update()
    {
        if (_isFocused && transform.localScale.x < 1)
        {
            _percent += Time.deltaTime * expandMultiply;
            SetScale(_percent);
        }
        else if (transform.localScale.x > 0)
        {
            _percent -= Time.deltaTime * colapseMultiply;
            SetScale(_percent);
        }
    }

    private void SetScale(float _timer)
    {
        _timer = Mathf.Clamp01(_timer);
        float scale = expandCurve.Evaluate(_timer);
        transform.localScale = Vector3.one * scale;
    }

    public void FocusImage()
    {
        _isFocused = true;
        if (_percent < 0.2f) _percent = 0.2f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _isFocused = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        UnfocusAll();
        FocusImage();
    }

    public static void UnfocusAll()
    {
        foreach (var image in Scaller)
            image._isFocused = false;
    }
}
