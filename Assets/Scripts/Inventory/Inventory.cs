﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public int SlotCount = 3;
    public InventorySlot SlotPrefab;
    public Transform SlotContainer;

    public List<InventorySlot> Slots = new List<InventorySlot>();

    protected void Start()
    {
        for (int i = 0; i < SlotCount; i++)
        {
            InventorySlot Slot = Instantiate(SlotPrefab, SlotContainer, false);
            Slots.Add(Slot);
        }
    }

    public InventorySlot GetFreeSlot()
    {
        for (int i = 0; i < Slots.Count; i++)
        {
            if (Slots[i].Item == null) return Slots[i];
        }
        return null;
    }

    public void SortInventory()
    {
        int i = 0;
        foreach (var slot in Slots)
        {
            if (slot.Item != null)
            {
                InventoryItem item = slot.Item;
                slot.Item = null;
                Slots[i].Item = item;
                item.Slot = Slots[i];
                i++;
            }
        }
    }
}
