﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(DelayedHoverClickIndicator))]
public class InventoryItem : MonoBehaviour, IPointerClickHandler, ISubmitHandler
{
    public int Id;
    public RectTransform Pivot;
    public float Speed = 3;
    public InventorySlot Slot { get; set; }
    public float SizeOffset = 25;

    public UnityEvent OnClick;

    Vector2 _startPosition;

    private DelayedHoverClickIndicator _delayedHover;
    private RectTransform _rt;
    private bool _anim = false;

    protected void Awake()
    {
        _delayedHover = GetComponent<DelayedHoverClickIndicator>();
        _rt = (RectTransform)transform;
        if (Pivot == null)
        {
            Pivot = (RectTransform)transform.parent;
        }
        else
        {
            _rt.SetParent(Pivot);
        }
    }

    private void Start()
    {
        _startPosition = ((RectTransform)transform).anchoredPosition;
    }

    protected void Update()
    {
        if (_anim)
        {
            if (Slot)
            {
                _rt.position = Vector3.Lerp(transform.position, Slot.transform.position, Time.deltaTime * Speed);
                if (_rt.position == Slot.transform.position)
                    _anim = false;
            }
            else
            {
                _rt.anchoredPosition = Vector3.Lerp(((RectTransform)transform).anchoredPosition, _startPosition, Time.deltaTime * Speed);
                if (_rt.anchoredPosition == _startPosition)
                    _anim = false;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnSubmit(eventData);
    }

    private void _attachToSlot()
    {
        Inventory inventory = FindObjectOfType<Inventory>();
        if (!inventory) return;
        InventorySlot slot = inventory.GetFreeSlot();
        if (!slot) return;
        Slot = slot;
        _rt.SetParent(Slot.ItemContainer);
        slot.Item = this;
        _delayedHover.hideItemPanelOnFocus = false;
    }

    private void _detachFromSlot()
    {
        Slot.Item = null;
        //Slot.Inventory.SortInventory();
        Slot = null;
        _rt.SetParent(Pivot);
        _delayedHover.hideItemPanelOnFocus = true;
    }

    public void OnSubmit(BaseEventData eventData)
    {
        if (Slot)
        {
            _detachFromSlot();
        }
        else
        {
            _attachToSlot();
        }
        _anim = true;
        OnClick.Invoke();
    }
}
