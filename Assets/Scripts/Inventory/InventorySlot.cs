﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour
{
    public Inventory Inventory { get; set; }
    public InventoryItem Item;
    public RectTransform ItemContainer;

    void Awake()
    {
        Inventory = GetComponentInParent<Inventory>();
        if (!ItemContainer) ItemContainer = (RectTransform)transform;
    }
}
