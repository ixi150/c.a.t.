﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInputModule : PointerInputModule
{
    public override Vector2 PointerPosition
    {
        get { return input.mousePosition; }
    }

    public override bool PointerDown
    {
        get { return input.GetMouseButtonDown(0); }
    }

    public override bool PointerUp
    {
        get { return input.GetMouseButtonUp(0); }
    }

    public override bool HoverPress
    {
        get { return true; }
    }
}
