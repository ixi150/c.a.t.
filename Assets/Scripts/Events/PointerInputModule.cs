﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class PointerInputModule : BaseInputModule
{
    public abstract Vector2 PointerPosition { get; }
    public abstract bool PointerDown { get; }
    public abstract bool PointerUp { get; }
    public abstract bool HoverPress { get; }

    public float ActionDelay = 1.5f;

    private HoverPointerEventData _pointerData;
    private float _lastChangeTime;

    public override void Process()
    {
        if (_pointerData == null) _pointerData = new HoverPointerEventData(eventSystem);

        _pointerData.delta = PointerPosition - _pointerData.position;
        _pointerData.position = PointerPosition;

        eventSystem.RaycastAll(_pointerData, m_RaycastResultCache);
        var hit = new RaycastResult();
        while (m_RaycastResultCache.Count > 0)
        {
            if (m_RaycastResultCache[0].gameObject.GetComponent<IPointerClickHandler>() != null)
            {
                hit = m_RaycastResultCache[0];
                break;
            }
            m_RaycastResultCache.RemoveAt(0);
        }
        _pointerData.pointerCurrentRaycast = hit;
        m_RaycastResultCache.Clear();

        if (_pointerData.pointerEnter != _pointerData.pointerCurrentRaycast.gameObject)
        {
            _pointerData.HoverStartTime = 0;
            _pointerExit();
            _pointerData.pointerEnter = _pointerData.pointerCurrentRaycast.gameObject;
            _pointerEnter();
            eventSystem.SetSelectedGameObject(_pointerData.pointerEnter);
        }
        if (_pointerData.pointerEnter)
        {
            _click();
        }
        
    }

    private void _click()
    {
        if (PointerDown)
        {
            _pointerData.pointerPress = _pointerData.pointerEnter;
            ExecuteEvents.Execute(_pointerData.pointerEnter, _pointerData, ExecuteEvents.pointerDownHandler);
        }
        if (PointerUp)
        {
            if (_pointerData.pointerPress == _pointerData.pointerCurrentRaycast.gameObject)
            {
                ExecuteEvents.Execute(_pointerData.pointerPress, _pointerData, ExecuteEvents.pointerClickHandler);
                _pointerData.HoverStartTime = Time.realtimeSinceStartup;
            }
            ExecuteEvents.Execute(_pointerData.pointerPress, _pointerData, ExecuteEvents.pointerUpHandler);
        }
        if (HoverPress)
        {
            if (_pointerData.Progress >= 1)
            {
                ExecuteEvents.Execute(_pointerData.pointerEnter, _pointerData, ExecuteEvents.submitHandler);
                _pointerData.HoverStartTime = Time.realtimeSinceStartup;
            }
            _sendOnPointerHover();
        }
    }

    private void _pointerExit()
    {
        if (_pointerData.pointerEnter != null)
        {
            ExecuteEvents.Execute(_pointerData.pointerEnter, _pointerData, ExecuteEvents.pointerExitHandler);
            _pointerData.HoverStartTime = Time.realtimeSinceStartup;
            if (HoverPress)
            {
                _sendOnPointerHover();
            }
        }
    }

    private void _pointerEnter()
    {
        if (_pointerData.pointerEnter != null)
        {
            ExecuteEvents.Execute(_pointerData.pointerEnter, _pointerData, ExecuteEvents.pointerEnterHandler);
            _pointerData.HoverStartTime = Time.realtimeSinceStartup;
        }
    }

    private void _sendOnPointerHover()
    {
        if(HoverPress) ExecuteEvents.Execute<IPointerOverHandler>(_pointerData.pointerEnter, _pointerData, (e, f) => { e.OnPointerHover(_pointerData); });
    }
}
