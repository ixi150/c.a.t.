﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HoverPointerEventData : PointerEventData
{
    public float HoverStartTime;

    public float Progress
    {
        get
        {
            var module = EventSystem.current.currentInputModule as PointerInputModule;
            if (module != null)
            {
                return Mathf.Clamp01((Time.realtimeSinceStartup - HoverStartTime) / module.ActionDelay);
            }
            return Mathf.Clamp01((Time.realtimeSinceStartup - HoverStartTime));
        }
    }

    public HoverPointerEventData(EventSystem eventSystem) : base(eventSystem)
    {
    }
}
