﻿using System.Collections;
using System.Collections.Generic;
using Tobii.Gaming;
using UnityEngine;
using UnityEngine.EventSystems;

public class TobiiInputModule : PointerInputModule
{
    public static TobiiInputModule Ref { get; private set; }

    public bool Enabled = true;

    public override Vector2 PointerPosition
    {
        get
        {
            if (_useMouse())
            {
                return Input.mousePosition;
            }
            return TobiiAPI.GetGazePoint().Screen;
        }
    }

    public override bool PointerDown
    {
        get { return Input.GetMouseButtonDown(0); }
    }

    public override bool PointerUp
    {
        get { return Input.GetMouseButtonUp(0); }
    }

    public override bool HoverPress
    {
        get { return !_useMouse(); }
    }

    public override bool IsActive()
    {
        return Enabled && TobiiAPI.GetUserPresence() == UserPresence.Present;
    }

    protected override void Awake()
    {
        base.Awake();
        if (Ref == null)
        {
            Ref = this;
        }
    }

    protected void Update()
    {
        if ((eventSystem.currentInputModule == this) != IsActive())
        {
            EventSystem system = eventSystem;
            system.gameObject.SetActive(false);
            system.gameObject.SetActive(true);

        }
    }


    private bool _useMouse()
    {
        return Input.GetMouseButton(0) || Input.GetMouseButtonUp(0);
    }
}
