﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;

public interface IPointerOverHandler : IEventSystemHandler
{
    void OnPointerHover(HoverPointerEventData data);
}
