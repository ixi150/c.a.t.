﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatorFloatController : MonoBehaviour
{
    [SerializeField]
    string floatName;

    [SerializeField]
    bool isGrowing;

    [SerializeField]
    float speed = 1;

    public bool IsGrowing
    {
        get { return isGrowing; }
        set { isGrowing = value; }
    }

    Animator anim;
    float timer;
    int hash;
    bool defaultIsGrowing;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        hash= Animator.StringToHash(floatName);
        defaultIsGrowing = isGrowing;
    }

    private void OnEnable()
    {
        isGrowing = defaultIsGrowing;
    }

    private void Update()
    {
        timer = Mathf.MoveTowards(timer, isGrowing ? 1 : 0, Time.deltaTime * speed);
        anim.SetFloat(hash, timer);
    }
}
