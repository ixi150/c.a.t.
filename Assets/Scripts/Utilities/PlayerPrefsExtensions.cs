﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefsExtensions
{
    public struct BoolPlayerPref
    {
        bool defaultValue;
        bool? catchedValue;
        string key;

        public BoolPlayerPref(string key, bool defaultValue = false)
        {
            this.key = key;
            this.catchedValue = null;
            this.defaultValue = defaultValue;
        }

        public bool Value
        {
            get
            {
                if (catchedValue == null)
                {
                    catchedValue = GetPlayerPrefsBool(key, defaultValue);
                }
                return catchedValue == true;
            }
            set
            {
                catchedValue = SetPlayerPrefsBool(key, value);
            }
        }
    }

    static bool SetPlayerPrefsBool(string key, bool value)
    {
        PlayerPrefs.SetInt(key, value ? 1 : 0);
        return value;
    }

    static bool GetPlayerPrefsBool(string key, bool defaultValue = false)
    {
        return PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) > 0;
    }
}

